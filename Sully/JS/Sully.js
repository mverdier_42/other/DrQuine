var i = 6;

function main() {
	// This define defines a function.
	if (i <= 0)
		return ;
	i--;
	const fs = require('fs');
	const exec = require('child_process').exec;
	fs.writeFile(`Sully_${i}.js`,
		`var i = ${i};\n\n`
		+ main.toString()
		+ ";\n\nmain();\n",
		function (err) {});
	if (i > 0)
		exec(`node Sully_${i}`);
};

main();
