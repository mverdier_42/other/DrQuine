%define i -1
section .text
global _main
extern _fopen
extern _fprintf
extern _sprintf
extern _system
extern _fclose
_main:
enter 16,0
mov r15,i
cmp r15,0
jle end
mov r14,i-1
lea rdi,[rel kid]
lea rsi,[rel kid_fmt]
mov rdx,r14
call _sprintf
lea rdi,[rel kid_exec]
lea rsi,[rel kid_exec_fmt]
mov rdx,r14
call _sprintf
lea rdi,[rel kid_obj]
lea rsi,[rel kid_obj_fmt]
mov rdx,r14
call _sprintf
lea rdi,[rel compil]
lea rsi,[rel compil_fmt]
lea rdx,[rel kid]
call _sprintf
lea rdi,[rel load]
lea rsi,[rel load_fmt]
lea rdx,[rel kid_obj]
lea rcx,[rel kid_exec]
call _sprintf
lea rdi,[rel clean]
lea rsi,[rel clean_fmt]
lea rdx,[rel kid_obj]
call _sprintf
lea rdi,[rel exec]
lea rsi,[rel exec_fmt]
lea rdx,[rel kid_exec]
call _sprintf
lea rdi,[rel kid]
lea rsi,[rel opt]
call _fopen
test rax,rax
jz end
mov rdi,rax
mov qword[rel file],rax
lea rsi,[rel code]
lea rdx,[rel code]
mov rcx,10
mov r8,34
mov r9,r14
call _fprintf
mov rdi,[rel file]
call _fclose
lea rdi,[rel compil]
call _system
lea rdi,[rel load]
call _system
lea rdi,[rel clean]
call _system
mov r9,r14
cmp r9,0
jle end
lea rdi,[rel exec]
call _system
end:
leave
ret
section .bss
file resq 1
kid resb 32
kid_exec resb 32
kid_obj resb 32
compil resb 64
load resb 128
clean resb 32
exec resb 32
section .data
kid_fmt db "Sully_%d.s", 0
kid_exec_fmt db "Sully_%d", 0
kid_obj_fmt db "Sully_%d.o", 0
compil_fmt db "nasm -f macho64 %s", 0
load_fmt db "ld %s -o %s -macosx_version_min 10.8 -lSystem", 0
clean_fmt db "rm -rf %s", 0
exec_fmt db "./%s", 0
opt db "w", 0
code db "%2$c%%define i %4$d%2$csection .text%2$cglobal _main%2$cextern _fopen%2$cextern _fprintf%2$cextern _sprintf%2$cextern _system%2$cextern _fclose%2$c_main:%2$center 16,0%2$cmov r15,i%2$ccmp r15,0%2$cjle end%2$cmov r14,i-1%2$clea rdi,[rel kid]%2$clea rsi,[rel kid_fmt]%2$cmov rdx,r14%2$ccall _sprintf%2$clea rdi,[rel kid_exec]%2$clea rsi,[rel kid_exec_fmt]%2$cmov rdx,r14%2$ccall _sprintf%2$clea rdi,[rel kid_obj]%2$clea rsi,[rel kid_obj_fmt]%2$cmov rdx,r14%2$ccall _sprintf%2$clea rdi,[rel compil]%2$clea rsi,[rel compil_fmt]%2$clea rdx,[rel kid]%2$ccall _sprintf%2$clea rdi,[rel load]%2$clea rsi,[rel load_fmt]%2$clea rdx,[rel kid_obj]%2$clea rcx,[rel kid_exec]%2$ccall _sprintf%2$clea rdi,[rel clean]%2$clea rsi,[rel clean_fmt]%2$clea rdx,[rel kid_obj]%2$ccall _sprintf%2$clea rdi,[rel exec]%2$clea rsi,[rel exec_fmt]%2$clea rdx,[rel kid_exec]%2$ccall _sprintf%2$clea rdi,[rel kid]%2$clea rsi,[rel opt]%2$ccall _fopen%2$ctest rax,rax%2$cjz end%2$cmov rdi,rax%2$cmov qword[rel file],rax%2$clea rsi,[rel code]%2$clea rdx,[rel code]%2$cmov rcx,10%2$cmov r8,34%2$cmov r9,r14%2$ccall _fprintf%2$cmov rdi,[rel file]%2$ccall _fclose%2$clea rdi,[rel compil]%2$ccall _system%2$clea rdi,[rel load]%2$ccall _system%2$clea rdi,[rel clean]%2$ccall _system%2$cmov r9,r14%2$ccmp r9,0%2$cjle end%2$clea rdi,[rel exec]%2$ccall _system%2$cend:%2$cleave%2$cret%2$csection .bss%2$cfile resq 1%2$ckid resb 32%2$ckid_exec resb 32%2$ckid_obj resb 32%2$ccompil resb 64%2$cload resb 128%2$cclean resb 32%2$cexec resb 32%2$csection .data%2$ckid_fmt db %3$cSully_%%d.s%3$c, 0%2$ckid_exec_fmt db %3$cSully_%%d%3$c, 0%2$ckid_obj_fmt db %3$cSully_%%d.o%3$c, 0%2$ccompil_fmt db %3$cnasm -f macho64 %%s%3$c, 0%2$cload_fmt db %3$cld %%s -o %%s -macosx_version_min 10.8 -lSystem%3$c, 0%2$cclean_fmt db %3$crm -rf %%s%3$c, 0%2$cexec_fmt db %3$c./%%s%3$c, 0%2$copt db %3$cw%3$c, 0%2$ccode db %3$c%1$s%3$c, 0%2$c", 0
