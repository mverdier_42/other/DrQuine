// Coucou ne fait rien.
function coucou() {
}

function main() {
	// Mais main fait des choses.
	coucou();
	console.log("// Coucou ne fait rien.\n"
		+ coucou.toString()
		+ "\n\n"
		+ main.toString()
		+ '\n\nmain();');

}

main();
