%define A 1
%define B 2
%define C 3
%macro main 0
call _main
_main:
enter 16,0
lea rdi,[rel kid]
lea rsi,[rel opt]
call _fopen
test rax,rax
jz end
mov rdi,rax
lea rsi,[rel code]
lea rdx,[rel code]
mov rcx,10
mov r8, 34
call _fprintf
end:
leave
ret
%endmacro
section .text
global start
global _main
extern _fopen
extern _fprintf
start:
main; Call main macro
ret
section .data
kid: db "Grace_kid.s", 0
opt: db "w", 0
code: db "%%define A 1%2$c%%define B 2%2$c%%define C 3%2$c%%macro main 0%2$ccall _main%2$c_main:%2$center 16,0%2$clea rdi,[rel kid]%2$clea rsi,[rel opt]%2$ccall _fopen%2$ctest rax,rax%2$cjz end%2$cmov rdi,rax%2$clea rsi,[rel code]%2$clea rdx,[rel code]%2$cmov rcx,10%2$cmov r8, 34%2$ccall _fprintf%2$cend:%2$cleave%2$cret%2$c%%endmacro%2$csection .text%2$cglobal start%2$cglobal _main%2$cextern _fopen%2$cextern _fprintf%2$cstart:%2$cmain; Call main macro%2$cret%2$csection .data%2$ckid: db %3$cGrace_kid.s%3$c, 0%2$copt: db %3$cw%3$c, 0%2$ccode: db %3$c%1$s%3$c%2$c"
