const define1 = 0;
const define2 = 1;
const define3 = function () {
	// This define defines a function.
	const fs = require('fs');
	fs.writeFile("Grace_kid.js",
		"const define1 = 0;\n"
		+ "const define2 = 1;\n"
		+ "const define3 = "
		+ define3.toString()
		+ ";\n\ndefine3();\n",
		function (err) {});
};

define3();
